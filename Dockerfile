# Copyright (C) 2019 Sebastian Schüpbach
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
# 
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

FROM archlinux/base AS build
RUN curl https://sh.rustup.rs -sSf | sh -s -- -y --no-modify-path --default-toolchain stable --default-host x86_64-unknown-linux-gnu \
&& source $HOME/.cargo/env
RUN pacman -S yarn

WORKDIR /build
RUN mkdir bin
ADD . .

WORKDIR /build/frontend/
RUN yarn run parcel build index.html
RUN cp dist/* ../bin 

WORKDIR /build/backend
RUN cargo build --release
RUN cp target/release/actix-with-elm /build/bin/app


FROM archlinux/base
WORKDIR /app
COPY --from=build /build/bin/* .
ENTRYPOINT /app/app