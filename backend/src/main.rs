// Copyright (C) 2019 Sebastian Schüpbach
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
// 
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#[macro_use]
extern crate actix_web;

use std::{env,io};

use actix_files as fs;
use actix_rt::System;
use actix_web::{App, HttpRequest, HttpResponse, HttpServer, Result};
use actix_web::guard;
use actix_web::http::{Method, StatusCode};
use actix_web::middleware;
use actix_web::web;

#[get("/favicon.ico")]
fn favicon() -> Result<fs::NamedFile> {
    Ok(fs::NamedFile::open("static/favicon.ico")?)
} 

#[get("/main.css")]
fn css() -> Result<fs::NamedFile> {
    Ok(fs::NamedFile::open("static/main.css")?)
} 

#[get("/elm.js")]
fn elm_js() -> Result<fs::NamedFile> {
    Ok(fs::NamedFile::open("js/elm.js")?)
}

fn p404() -> Result<fs::NamedFile> {
    Ok(fs::NamedFile::open("static/404.html")?.set_status_code(StatusCode::NOT_FOUND))
}


#[get("/")]
fn webapp() -> Result<fs::NamedFile> {
    Ok(fs::NamedFile::open("static/index.html")?)
}



fn index(req: &HttpRequest) -> HttpResponse {
    println!("{:?}", req);

    // session
/*     let mut counter = 1;
    if let Some(count) = req.session().get::<i32>("counter")? {
        println!("SESSION value: {}", count);
        counter = count + 1;
    } */

    // set counter to session
    // req.session().set("counter", counter)?;

    // response
    HttpResponse::build(StatusCode::OK)
        .content_type("text/html; charset=utf-8")
        .body(include_str!("../static/index.html"))
}

fn main() -> io::Result<()>  {
    // Start logger
    env::set_var("RUST_LOG", "actix_web=debug");
    env_logger::init();

    // Create actor runtime
    let sys = System::new("actix-with-elm");

    HttpServer::new(|| {
        App::new()
            .wrap(middleware::Logger::default())
            .service(elm_js)
            .service(css)
            .service(favicon)
            .service(webapp)
            .default_service(
                // 404 for GET request
                web::resource("")
                    .route(web::get().to(p404))
                    // all requests that are not `GET`
                    .route(
                        web::route()
                            .guard(guard::Not(guard::Get()))
                            .to(|| HttpResponse::MethodNotAllowed()),
                    ),
)
    })
    .bind("127.0.0.1:8088")?
    .start();

    // Start actor system
    sys.run()
}
