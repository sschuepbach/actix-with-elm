#!/bin/bash

SCRIPTPATH="$( cd "$(dirname "$0")" ; pwd -P )"
SOURCE="$SCRIPTPATH/frontend/dist"
BACKEND="$SCRIPTPATH/backend"

cd $SOURCE
JSFILE=`ls -t *.js | head -1`
mv $JSFILE $BACKEND/js/elm.js
CSS=`ls -t *.css | head -1`
mv $CSS $BACKEND/static/main.css
FAVICON=`ls -t favicon.*.ico | head -1`
mv $FAVICON $BACKEND/static/favicon.ico
sed -i "s/\/frontend.*js/\/elm.js/" index.html
sed -i "s/\/main.*css/\/main.css/" index.html
sed -i "s/\/favicon.*ico/\/favicon.ico/" index.html
mv index.html $BACKEND/static/
