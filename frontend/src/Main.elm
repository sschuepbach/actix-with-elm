-- Copyright (C) 2019 Sebastian Schüpbach
-- 
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU Affero General Public License as
-- published by the Free Software Foundation, either version 3 of the
-- License, or (at your option) any later version.
-- 
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU Affero General Public License for more details.
-- 
-- You should have received a copy of the GNU Affero General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.

module Main exposing (Msg(..), main, update, view)

import Browser
import Html exposing (Html, button, div, h2, img, text)
import Html.Attributes exposing (id, src, style)
import Html.Events exposing (onClick)
import Http
import Json.Decode exposing (Decoder, field, string)


main =
    Browser.element
        { init = init
        , update = update
        , subscriptions = subscriptions
        , view = view
        }


type Model
    = Failure
    | Loading
    | Unloading
    | Success String


init : () -> ( Model, Cmd Msg )
init _ =
    ( Loading, getRandomCatGif )


type Msg
    = MorePlease
    | GotGif (Result Http.Error String)


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        MorePlease ->
            ( Loading, getRandomCatGif )

        GotGif result ->
            case result of
                Ok url ->
                    ( Success url, Cmd.none )

                Err _ ->
                    ( Failure, Cmd.none )


subscriptions : Model -> Sub Msg
subscriptions model =
    Sub.none


view : Model -> Html Msg
view model =
    div []
        [ h2 [] [ text "Random Cats" ]
        , viewGif model
        ]


viewGif : Model -> Html Msg
viewGif model =
    case model of
        Failure ->
            div [] [ text "I could not load a random cat for some reason.", button [ onClick MorePlease ] [ text "Try again!" ] ]

        Loading ->
            text "Loading..."

        Unloading ->
            text "Unloading..."

        Success url ->
            div [] [ button [ onClick MorePlease, style "display" "block" ] [ text "More Please!" ], img [ src url ] [] ]


getRandomCatGif : Cmd Msg
getRandomCatGif =
    Http.get { url = "https://api.giphy.com/v1/gifs/random?api_key=dc6zaTOxFJmzC&tag=cat", expect = Http.expectJson GotGif gifDecoder }


gifDecoder : Decoder String
gifDecoder =
    field "data" (field "image_url" string)
